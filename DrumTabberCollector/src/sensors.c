/*
 * sensors.c
 *
 * Created: 7/05/2015 8:43:57 AM
 *  Author: Pearce
 */ 


#include "headers/sensors.h"
#include <string.h>


#define NUM_CHANNELS    (2)						// Total number of ADC channels in use
#define ADC_DONE_MASK   ((1<<NUM_CHANNELS)-1)	// ADC convention done mask
#define BUFFER_SIZE     NUM_CHANNELS			// Size of the receive buffer and transmit buffer
#define VOLT_REF        (3300)					// Reference voltage for ADC (mV)
#define MAX_DIGITAL     (4095)					// maximum value of a 12-bit number

#define MAX_SENSORS		5
#define USE_SEQUENCER	true
#define ADC_CLK			2000000UL

static volatile Bool adc_pdc_enabled = false;
static volatile Bool auto_cal_enabled = true;

enum adc_channel_num_t ch_list[MAX_SENSORS] = 
{
	ADC_CHANNEL_0,			// PA17 -> Header J2.5
	ADC_CHANNEL_8,			// PA21 -> Header J2.6
	ADC_CHANNEL_10,			// PC13 -> Header J2.7
	ADC_CHANNEL_11,			// PC15 -> Header J2.8
	ADC_TEMPERATURE_SENSOR
};

struct adc_data_t {
	uint8_t channels[NUM_CHANNELS];
	uint16_t values[NUM_CHANNELS];
	uint16_t is_done;
} adc_data;

struct adc_raw_t {
	uint16_t values[NUM_CHANNELS];	
} buffer[32];

uint8_t buffer_count = 0;
Bool rdy_to_send = false;

/**
 * Read converted data through PDC channel
 *	-> adc:	The pointer of adc peripheral
 *	-> dest:	The destination buffer
 *	-> size:	The size of the buffer
 */
static uint32_t adc_read_buffer(Adc *adc, uint16_t *dest, uint32_t size)
{
	// Check if the first PDC bank is free
	if ((adc->ADC_RCR == 0) && (adc->ADC_RNCR == 0)) 
	{
		adc->ADC_RPR = (uint32_t)dest;
		adc->ADC_RCR = size;
		adc->ADC_PTCR = ADC_PTCR_RXTEN;
		return 1;
	} 
	else 
	{ 
		if (adc->ADC_RNCR == 0) 
		{
			adc->ADC_RNPR = (uint32_t)dest;
			adc->ADC_RNCR = size;
			return 1;
		} 
		return 0;
	}
}

void ADC_Handler(void)
{
	uint32_t i;
	uint32_t tmp;
	uint8_t ch;

	/* With PDC transfer */
	if (adc_pdc_enabled) 
	{
		if ((adc_get_status(ADC) & ADC_ISR_RXBUFF) == ADC_ISR_RXBUFF) 
		{
			adc_data.is_done = ADC_DONE_MASK;
			adc_read_buffer(ADC, buffer[buffer_count].values, BUFFER_SIZE);
			
			// Only keep sample value, and discard channel number
			for (i = 0; i < NUM_CHANNELS; i++) 
			{
				//adc_data.values[i] &= ADC_LCDR_LDATA_Msk;
				buffer[buffer_count].values[i] &= ADC_LCDR_LDATA_Msk;
			}
			
			if (++buffer_count > 32)
			{
				rdy_to_send = true;
			}
		}
	} 
	/* Without PDC transfer */
	else if ((ADC->ADC_ISR & ADC_ISR_DRDY) == ADC_ISR_DRDY) 
	{
		tmp = adc_get_latest_value(ADC);
		
		for (i = 0; i < NUM_CHANNELS; i++) 
		{
			ch = (tmp & ADC_LCDR_CHNB_Msk) >> ADC_LCDR_CHNB_Pos;
			if (adc_data.channels[i] == ch) 
			{
				//adc_data.values[i][count++] = tmp & ADC_LCDR_LDATA_Msk;
				buffer[buffer_count].values[i] = tmp & ADC_LCDR_LDATA_Msk;
				//adc_data.is_done |= 1 << i;
			}
			
			if (ch == adc_data.channels[NUM_CHANNELS - 1])
			{
				if (++buffer_count > 32)
				{
					rdy_to_send = true;
				}
			}
		}
	}	
}

// initialise the sensors (adc's)
void sensors_init(Bool use_pdc)
{
	adc_pdc_enabled = use_pdc;
	buffer_count = 0;
	pmc_enable_periph_clk(ID_ADC);
	
	/*
	 * Formula: ADCClock = MCK / ( (PRESCAL+1) * 2 )
	 * MCK = 120MHZ, PRESCAL = 29, then:
	 * ADCClock = 120 / ((29 + 1) * 2) = 2MHz;
	 *
	 * Formula:
	 *     Startup  Time = startup value / ADCClock
	 *     Startup time = 64 / 2MHz = 32 us
	 */
	adc_init(ADC, sysclk_get_cpu_hz(), 2000000, ADC_STARTUP_TIME_4);
	memset((void *)&adc_data, 0, sizeof(adc_data));
		
	/* ADC timing Formula:
	 *     Transfer Time = (TRANSFER * 2 + 3) / ADCClock = (1 * 2 + 3) / 6.4MHz = 781 ns
	 *     Tracking Time = (TRACKTIM + 1) / ADCClock = (1 + 1) / 6.4MHz = 312 ns
	 *     Settling Time = settling value / ADCClock = 3 / 6.4MHz = 469 ns
	 */
	adc_configure_timing(ADC, 1, ADC_SETTLING_TIME_3, 1);	
	adc_enable_tag(ADC);
	
	//adc_configure_sequence(ADC, channels, 1);
	adc_stop_sequencer(ADC);			
	for (int i = 0; i < MAX_SENSORS; ++i)
	{
		adc_enable_channel(ADC, ch_list[i]);
		adc_data.channels[i] = ch_list[i];
	}
	
	adc_enable_ts(ADC);
	adc_disable_anch(ADC);
	for (int i = 0; i < MAX_SENSORS; ++i)
	{
		adc_set_channel_input_gain(ADC, ch_list[i], ADC_GAINVALUE_0);
		adc_disable_channel_input_offset(ADC, ch_list[i]);
	}
	
	/* Set Auto Calibration Mode. */
	if (auto_cal_enabled) 
	{
		adc_set_calibmode(ADC);
		while (1) 
		{
			if ((adc_get_status(ADC) & ADC_ISR_EOCAL) == ADC_ISR_EOCAL)
			{
				break;
			}
		}
	}
	
	adc_configure_power_save(ADC, 0, 0);
	
	/* Transfer with/without PDC. */
	if (adc_pdc_enabled) 
	{
		adc_read_buffer(ADC, adc_data.values, BUFFER_SIZE);
		/* Enable PDC channel interrupt. */
		adc_enable_interrupt(ADC, ADC_IER_RXBUFF);
	}
	else 
	{
		/* Enable Data ready interrupt. */
		adc_enable_interrupt(ADC, ADC_IER_DRDY);
	}
	/* Enable ADC interrupt. */
	NVIC_EnableIRQ(ADC_IRQn);
	
	adc_configure_trigger(ADC, ADC_TRIG_SW, 0);
}

uint16_t *sensors_get_data(uint8_t *len)
{
	uint16_t *ret = membag_alloc(sizeof(uint16_t) * NUM_CHANNELS * buffer_count);
	memcpy(ret, buffer, sizeof(uint16_t) * NUM_CHANNELS * buffer_count);
	*len = buffer_count;
	
	for (int i = 0; i < buffer_count; ++i)
	{
		for (int j = 0; j < NUM_CHANNELS; ++j)
		{
			ret[j + (i * NUM_CHANNELS)] = buffer[i].values[j];
		}
	}
	buffer_count = 0;
	return ret;
}

inline uint8_t sensors_data_count(void)
{
	return buffer_count;
}

void sensors_start(void)
{
	adc_start(ADC);
}
