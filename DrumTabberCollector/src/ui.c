#include <asf.h>
#include "headers/ui.h"
#include "config/conf_drumtabber.h"

volatile Bool btn_pressed = false;

static void button_handler(uint32_t id, uint32_t mask)
{
	if ((PUSH_BUTTON_ID == id) && (PUSH_BUTTON_PIN_MSK == mask)) 
	{
		btn_pressed = true;
	}
}

void ui_init(void)
{ 
	pmc_enable_periph_clk(PUSH_BUTTON_ID);								// Configure PIO clock.
	
	pio_set_debounce_filter(PUSH_BUTTON_PIO, PUSH_BUTTON_PIN_MSK, 10);	// Adjust PIO debounce filter parameters, uses 10 Hz filter. 
	
	pio_handler_set(PUSH_BUTTON_PIO, PUSH_BUTTON_ID, 					// Initialize PIO's interrupt handlers, see PIO definition in board.h	
		PUSH_BUTTON_PIN_MSK, PUSH_BUTTON_ATTR, button_handler);

	NVIC_EnableIRQ((IRQn_Type)PUSH_BUTTON_ID);							// Enable PIO controller IRQs
	
	pio_enable_interrupt(PUSH_BUTTON_PIO, PUSH_BUTTON_PIN_MSK);			// Enable PIO line interrupts. 
	
	// Initialize LEDs
	LED_On(LED0_GPIO);
	LED_Off(LED1_GPIO);
}

void ui_powerdown(void)
{
	LED_Off(LED0_GPIO);
	LED_Off(LED1_GPIO);
}

void ui_wakeup(void)
{
	LED_On(LED0_GPIO);
}

void ui_com_open(uint8_t port)
{
	UNUSED(port);
	LED_On(LED1_GPIO);
}

void ui_com_close(uint8_t port)
{
	UNUSED(port);
	LED_Off(LED1_GPIO);
}

void ui_com_rx_start(void)
{
}

void ui_com_rx_stop(void)
{
}

void ui_com_tx_start(void)
{
}

void ui_com_tx_stop(void)
{
}

void ui_com_error(void)
{

}

void ui_com_overflow(void)
{

}

void ui_process(uint16_t framenumber)
{
	if (0 == framenumber) {
		LED_On(LED0_GPIO);
	}
	if (1000 == framenumber) {
		LED_Off(LED0_GPIO);
	}
}

void ui_all(Bool on)
{
	if (on) { LED_On(LED0_GPIO); LED_On(LED1_GPIO);	} 
	else	{ LED_Off(LED0_GPIO); LED_Off(LED1_GPIO); }
}

Bool ui_get_btn(void)
{
	Bool tmp = btn_pressed;
	btn_pressed = false;
	
	return tmp;
}
