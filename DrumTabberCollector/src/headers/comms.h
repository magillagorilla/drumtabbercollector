/*
 * comms.h
 *
 * Created: 7/05/2015 7:38:42 AM
 *  Author: Pearce
 */ 


#ifndef COMMS_H_
#define COMMS_H_


/*! \brief Called by CDC interface
 * Callback running when CDC device have received data
 */
void usb_data_recieved(uint8_t port);

/*! \brief Configures communication line
 *
 * \param cfg      line configuration
 */
void uart_config(uint8_t port, usb_cdc_line_coding_t * cfg);

/*! \brief Opens communication line
 */
void uart_open(uint8_t port);

/*! \brief Closes communication line
 */
void uart_close(uint8_t port);


void usb_write_batch(uint16_t *data, uint8_t len);


int usb_write_line(char *line);

int usb_write(char *line);

void usb_newline(void);

int usb_get_buffer(char *buff, int max_len);

Bool usb_line_received(void);

#endif /* COMMS_H_ */