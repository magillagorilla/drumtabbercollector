/*
 *	ui.h
 *
 *	Created: 7/05/2015 7:40:19 AM
 *  Author: Pearce
 */


#ifndef UI_H_
#define UI_H_

// Initializes the user interface
void ui_init(void);

// Enters the user interface in power down mode
void ui_powerdown(void);

// Exits the user interface of power down mode
void ui_wakeup(void);

// Called when communication port is opened
void ui_com_open(uint8_t port);

// Called when communication port is closed
void ui_com_close(uint8_t port);

// Called when a data is received on CDC
void ui_com_rx_start(void);

// Called when a data is received on port com
void ui_com_tx_start(void);

// Called when all data pending are sent on port com
void ui_com_rx_stop(void);

// Called when all data pending are sent on CDC
void ui_com_tx_stop(void);

// Called when a communication error occur
void ui_com_error(void);

// Called when a overflow occur
void ui_com_overflow(void);

// This process is called every 1ms while the USB interface is enabled.
//		=> framenumber: The current frame number
void ui_process(uint16_t framenumber);


// Change the value of all the LED's -> 'on' indicates whether to turn them on/off
void ui_all(Bool on);

Bool ui_get_btn(void);

#endif // UI_H_