/*
 *	main.h
 *
 *	Created: 7/05/2015 7:38:01 AM
 *  Author: Pearce
 */  

#ifndef MAIN_H_
#define MAIN_H_

#include "usb_protocol_cdc.h"


#pragma region USB CDC Functions
 
// Opens the communication port -> called by CDC interface when USB Host enable it.
//		=> returns true if cdc startup is successfully done
Bool main_cdc_enable(uint8_t port);

// Closes the communication port -> called by CDC interface when USB Host disable it.
void main_cdc_disable(uint8_t port);

// Manages the leds behaviors -> Called when a start of frame is received on USB line each 1ms.
void main_sof_action(void);

// Enters the application in low power mode -> called when USB host sets USB line in suspend state
void main_suspend_action(void);

// Turn on a led to notify active mode -> called when the USB line is resumed from the suspend state
void main_resume_action(void);

// Save new DTR state to change led behavior. The DTR notify that the terminal have open or close the communication port. 
void main_cdc_set_dtr(uint8_t port, Bool b_enable);

#pragma endregion USB CDC Functions

void parse_cmd(char *buff, int len);

#endif // MAIN_H_ 