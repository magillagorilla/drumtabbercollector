/*
 * sensors.h
 *
 * Created: 7/05/2015 8:44:06 AM
 *  Author: Pearce
 */ 


#ifndef SENSORS_H_
#define SENSORS_H_

#include <asf.h>

void sensors_init(Bool use_pdc);

void sensors_start(void);

uint16_t *sensors_get_data(uint8_t *len);
//uint8_t sensors_get_data(uint16_t **data, uint8_t max);

uint8_t sensors_data_count(void);

#endif /* SENSORS_H_ */