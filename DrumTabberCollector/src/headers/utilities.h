/*
 * utilities.h
 *
 * Created: 7/05/2015 1:46:20 PM
 *  Author: Pearce
 */ 


#ifndef UTILITIES_H_
#define UTILITIES_H_

#include <asf.h>

#define True	true
#define False	false
#define ON		true
#define OFF		false

typedef Bool boolean;

void sram_init(void);
uint32_t sram_test(uint32_t length);

void matrix_init(void);

char* itoa(int value, char* result, int base);

char *uitoa(char* buf, uint32_t n);

uint32_t power(uint32_t x, uint32_t y);

#endif /* UTILITIES_H_ */