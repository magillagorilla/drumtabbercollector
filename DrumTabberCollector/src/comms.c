/*
 * comms.c
 *
 * Created: 7/05/2015 7:38:35 AM
 *  Author: Pearce
 */ 

#include <asf.h>
#include "conf_drumtabber.h"
#include "headers/comms.h"
#include "headers/main.h"
#include "headers/ui.h"
#include "headers/utilities.h"
#include <string.h>

//static sam_usart_opt_t usart_options;

char buffer[64];
int count = 0;
Bool line_recv = false;

void usb_data_recieved(uint8_t port)
{
	if (udi_cdc_is_rx_ready())
	{
		char received[64] = { NULL };
		int num = udi_cdc_get_nb_received_data();
		int len = udi_cdc_read_buf(received, num);
		
		while (len != 0) 
		{
			int cc = udi_cdc_get_nb_received_data();
			len = udi_cdc_read_buf(&(received[num]), cc);
			num += cc;
		}
		
		memset(buffer, 0, sizeof(buffer));
		memcpy(buffer, received, num);
		
		line_recv = true;
		count = num;
	}
}

void uart_config(uint8_t port, usb_cdc_line_coding_t * cfg)
{
	UNUSED(port);
	UNUSED(cfg);
}

void usb_write_batch(uint16_t *data, uint8_t len)
{
	char buffer[256];
	memset(buffer, 0x00, sizeof(buffer));
	
	for (uint8_t i = 0; i < len; ++i)
	{
		char tmp[8];
		memset(tmp, 0x00, sizeof(tmp));
		
		uitoa(tmp, (uint32_t)data[i]);
		strcat(buffer, tmp);
		
		strcat(buffer, i < len - 1 ? "," : "\r");
	}
	
	size_t total = strlen(buffer); 
	udi_cdc_write_buf(buffer, total);
}

int usb_write_line(char *line)
{
	while (!udi_cdc_is_tx_ready());
	
	int len = strlen(line);
	udi_cdc_write_buf(line, len);
	udi_cdc_putc('\r');
	return len + 1;
}

int usb_write(char *line)
{
	while (!udi_cdc_is_tx_ready());
	
	return udi_cdc_write_buf(line, strlen(line));
}

void usb_newline(void)
{
	udi_cdc_putc('\r');
}

Bool usb_line_received(void)
{
	return line_recv;
}

int usb_get_buffer(char *buff, int max_len)
{
	memset(buff, 0, max_len);
	memcpy(buff, buffer, count < max_len ? count : max_len);

	line_recv = false;
	
	return count;
}
