/*
 * utilities.c
 *
 * Created: 7/05/2015 2:02:34 PM
 *  Author: Pearce
 */ 

#include "headers/utilities.h"

#define MATRIX_SLAVE_NUM    5
#define MATRIX_MASTER_NUM	4

#define SRAM_PAGE_SIZE		(512000UL)
#define SRAM_PAGE_COUNT		(16)
#define SRAM_SIZE			(SRAM_PAGE_SIZE * SRAM_PAGE_COUNT)

static void sram_configure(uint32_t cs)
{
	smc_set_setup_timing(SMC, cs, SMC_SETUP_NWE_SETUP(1)
		| SMC_SETUP_NCS_WR_SETUP(1)
		| SMC_SETUP_NRD_SETUP(1)
		| SMC_SETUP_NCS_RD_SETUP(1));
	smc_set_pulse_timing(SMC, cs, SMC_PULSE_NWE_PULSE(6)
		| SMC_PULSE_NCS_WR_PULSE(6)
		| SMC_PULSE_NRD_PULSE(6)
		| SMC_PULSE_NCS_RD_PULSE(6));
	smc_set_cycle_timing(SMC, cs, SMC_CYCLE_NWE_CYCLE(7)
		| SMC_CYCLE_NRD_CYCLE(7));
	smc_set_mode(SMC, cs, SMC_MODE_READ_MODE | SMC_MODE_WRITE_MODE);
}

void sram_init( void )
{	
	pmc_enable_periph_clk(ID_SMC);	// Enable PMC clock for SMC 
	sram_configure(SRAM_CHIP_SELECT);
	
	pio_configure_pin(PIN_EBI_NLB, PIN_EBI_NLB_FLAGS); /* Configure LB, enable SRAM access */
}

// tests 'length' bytes of the external SRAM memory
uint32_t sram_test( uint32_t length )
{
	uint32_t i;
	uint8_t *ptr = (uint8_t *)(SRAM_BASE_ADDRESS);
	
	// clear SRAM area
	for (i = 0; i < length; ++i)
	{
		ptr[i] = 0xff;
	}
	
	// write some test values to the SRAM
	for (i = 0; i < length; ++i)
	{
		if (i & 1)	{ ptr[i] = ((0x5a & i) & 0xff); }
		else		{ ptr[i] = ((0xa5 & i) & 0xff); }
	}
	
	// read-back and check each value
	for (i = 0; i < length; ++i)
	{
		if (i & 1)
		{
			if (ptr[i] != ((0x5a & i) & 0xff)) { return i; }
		}
		else
		{
			if (ptr[i] != ((0xa5 & i) & 0xff)) { return i; }
		}
	}
	
	return length;
}

void matrix_init( void )
{
	uint32_t slave, master;
	
	for (slave = 0; slave < MATRIX_SLAVE_NUM; ++slave)
	{
		matrix_set_slave_arbitration_type(slave, MATRIX_ARBT_ROUND_ROBIN);
		matrix_set_slave_default_master_type(slave, MATRIX_DEFMSTR_LAST_DEFAULT_MASTER);
	}
	
	for (master = 0; master < MATRIX_MASTER_NUM; ++master)
	{
		matrix_set_master_burst_type(master, MATRIX_ULBT_16_BEAT_BURST);
	}
}

#pragma region ---------- Helper Functions ------------------------------------

static char *ltoa(long value, char *result, int base)
{
	// check that the base if valid
	if (base < 2 || base > 36) { *result = '\0'; return result; }
	
	char* ptr = result, *ptr1 = result, tmp_char;
	long tmp_value;
	
	do {
		tmp_value = value;
		value /= base;
		*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
	} while ( value );
	
	// Apply negative sign
	if (tmp_value < 0) *ptr++ = '-';
	*ptr-- = '\0';
	while(ptr1 < ptr) {
		tmp_char = *ptr;
		*ptr--= *ptr1;
		*ptr1++ = tmp_char;
	}
	return result;
}

char* itoa(int value, char* result, int base)
{
	return ltoa((long)value, result, base);
}

char *uitoa(char* buf, uint32_t n)
{
	return ltoa((long)n, buf, 10);
}

uint32_t power(uint32_t x, uint32_t y)
{
	if (y == 0) return 1;
	uint32_t ret = x;
	for (int i = 0; i < (int)(y - 1); i++)
	{
		ret *= x;
	}
	return ret;
}

#pragma endregion /HelperFunctions
/*
char *uitoa(char* buf, uint32_t n)
{
	static const char digit[] = {"0123456789"};
	uint32_t number = n;

	uint32_t digits = get_digits(n);
	for (int i = digits; i >= 1; i--)
	{
		uint32_t tmp = (uint32_t)((number / power(10, i - 1)));
		buf[digits - i] = digit[tmp];
		number -= tmp * power(10, i - 1);
	}
	buf[digits] = '\0';
	return buf;
}

uint32_t get_digits(uint32_t n)
{
	uint32_t number = n;
	uint32_t digits = 0;
	for (digits = 1; 1; digits++)
	{
		number /= 10;
		if (number == 0) break;
	}
	return digits;
}

uint32_t power(uint32_t x, uint32_t y)
{
	if (y == 0) return 1;
	uint32_t ret = x;
	for (int i = 0; i < (int)(y - 1); i++)
	{
		ret *= x;
	}
	return ret;
}
*/