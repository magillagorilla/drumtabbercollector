#include <asf.h>
#include "conf_usb.h"
#include "headers/ui.h"
#include "headers/comms.h"
#include "headers/sensors.h"
#include "headers/utilities.h"

#include <string.h>


static volatile bool main_cdc_enabled = false;

static volatile uint16_t buffer[32];
static volatile uint8_t length = 0;

/** Global timestamp in milliseconds since start of application */
static volatile uint32_t ms_ticks = 0;

void SysTick_Handler(void)
{
	ms_ticks++; 
}

static void msdelay(uint32_t ms)
{
	uint32_t time = ms_ticks;
	while ((ms_ticks - time) < ms);
}

/*! \brief Main function. Execution starts here.
 */
int main(void)
{
	wdt_disable(WDT);
	
	irq_initialize_vectors();
	cpu_irq_enable();

	// Initialize the sleep manager
	sleepmgr_init();
	sysclk_init();
	board_init();
	membag_init();
		
	sensors_init(True);	
	udc_start();		// Start USB stack to authorize VBus monitoring
	
	ui_init();
	ui_powerdown();

	Bool systick_fail = false;
	if (SysTick_Config(sysclk_get_cpu_hz() / 1000))
	{
		ui_all(OFF);
		systick_fail = true; // error configuring systick
	}
	
	
	// loop manages only the power mode coz the USB management is done by interrupt	
	char buff[32];
	while (true) 
	{
		if (ui_get_btn())
		{
			if (systick_fail)
			{
				usb_write_line("SysTick Setup Failed...");
				systick_fail = false;
			}
			
			memset(buff, 0, sizeof(buff));
			uitoa(buff, ms_ticks);
			strcat(buff, "ms");
			
			usb_write("TERM,Push-Button Pressed... ");				
			usb_write_line(buff);
		}
		if (usb_line_received())
		{
			char tmp[64];
			int len = usb_get_buffer(tmp, 64);
			
			parse_cmd(tmp, len);
		}
	}
}
/*
 if (sensors_data_count() < 32)
 {
	 adc_start(ADC);
	 msdelay(25);
 }
 else
 {
	 uint16_t *data = sensors_get_data(&length);
	 usb_write_batch(data, length);
	 membag_free(data);
 }
 */

/*
uint32_t cpu = sysclk_get_cpu_hz();
uint32_t mck = sysclk_get_main_hz();
uint32_t periph = sysclk_get_peripheral_bus_hz(ADC);

char str_cpu[32] = { NULL };
char str_mck[32] = { NULL };
char str_periph[32] = { NULL };

uitoa(str_cpu, cpu);
uitoa(str_mck, mck);
uitoa(str_periph, periph);

udi_cdc_putc('\n'); udi_cdc_putc('\r');
usb_write_line("CPU CLK: "); usb_write(str_cpu); usb_write_line("Hz");
usb_write_line("MCK CLK: "); usb_write(str_mck); usb_write_line("Hz");
usb_write_line("PER CLK: "); usb_write(str_periph); usb_write_line("Hz");


//sleepmgr_enter_sleep();
*/

void main_suspend_action(void)
{
	ui_powerdown();
}

void main_resume_action(void)
{
	ui_wakeup();
}

void main_sof_action(void)
{
	if (!main_cdc_enabled)
		return;
	ui_process(udd_get_frame_number());
}

Bool main_cdc_enable(uint8_t port)
{
	main_cdc_enabled = true;
	// Open communication
	//uart_open(port);
	return true;
}

void main_cdc_disable(uint8_t port)
{
	main_cdc_enabled = false;
	// Close communication
	//uart_close(port);
}

void main_cdc_set_dtr(uint8_t port, Bool b_enable)
{
	if (b_enable) {
		// Host terminal has open COM
		ui_com_open(port);
	}else{
		// Host terminal has close COM
		ui_com_close(port);
	}
}

void parse_cmd(char *buff, int len)
{
	int cr = 0;
	while (buff[cr] != '\r' && cr < len)
	{
		++cr;
	}
	
	int comma = 0;
	while (buff[comma] != ',' && comma < len) { ++comma; }
	
	char cmd[8] = { NULL };
	memcpy(cmd, buff, comma);
	
	if (strcmp(cmd, "TERM") == 0)
	{
		usb_write_line("TERM,OK\r");
	} 
	else if (strcmp(cmd, "REC") == 0)
	{
		if (buff[comma + 1] == '1')
		{
			// start recording
		}
		else
		{
			// stop recording
		}
	}
}
