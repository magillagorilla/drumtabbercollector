/*
 * conf_drumtabber.h
 *
 * Created: 7/05/2015 7:41:18 AM
 *  Author: Pearce
 */ 


#ifndef CONF_DRUMTABBER_H_
#define CONF_DRUMTABBER_H_

#define USART_ENABLE()
#define USART_DISABLE()
#define USART_BASE       ((Usart*)UART0)
#define USART_ID         ID_UART0

#define USART_HANDLER    UART0_Handler
#define USART_INT_IRQn   UART0_IRQn
#define USART_INT_LEVEL  3


/** Push button pin definition. */
#define PUSH_BUTTON_PIO          PIOA
#define PUSH_BUTTON_ID           ID_PIOA
#define PUSH_BUTTON_PIN_MSK      (1 << 5)
#define PUSH_BUTTON_ATTR         (PIO_PULLUP | PIO_DEBOUNCE | PIO_IT_RISE_EDGE)

/** The erase pin mask value in the PIO mode and erase mode. */
#define PIN_PIO_MODE_MSK         CCFG_SYSIO_SYSIO12
#define PIN_ERASE_MODE_MSK       0


#endif /* CONF_DRUMTABBER_H_ */